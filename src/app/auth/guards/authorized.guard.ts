import { Injectable } from '@angular/core';
import { CanLoad, Route, Router, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from '../services/auth.service';
import { SessionStorageService } from '../services/session-storage.service';

@Injectable()
export class AuthorizedGuard implements CanLoad {
  constructor(
    private authService: AuthService,
    private router: Router,
    private sessionStorageService: SessionStorageService
  ) {}

  canLoad(
    route: Route
  ):
    | Observable<boolean | UrlTree>
    | Promise<boolean | UrlTree>
    | boolean
    | UrlTree {
    let isAuthorised = !!this.sessionStorageService.getToken();
    this.authService.isAuthorised$.subscribe((res) => {
      isAuthorised = res;
    });
    console.log('can load works .... isAuthorised ', isAuthorised);
    if (!isAuthorised) {
      return this.router.parseUrl('/login');
    }
    return isAuthorised;
  }
}
