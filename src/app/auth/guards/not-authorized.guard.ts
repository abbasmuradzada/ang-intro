import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  CanActivate,
  Router,
  RouterStateSnapshot,
  UrlTree,
} from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from '../services/auth.service';

@Injectable()
export class NotAuthorizedGuard implements CanActivate {
  constructor(private authService: AuthService, private router: Router) {}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ):
    | Observable<boolean | UrlTree>
    | Promise<boolean | UrlTree>
    | boolean
    | UrlTree {
    let notAuthorised = true;
    // I thinks it needs to work asynchronously (not sure, can come back to here)
    this.authService.isAuthorised$.subscribe((res) => {
      notAuthorised = !res;
    });
    if (!notAuthorised) {
      return this.router.parseUrl('/courses');
    }
    return notAuthorised;
  }
}
