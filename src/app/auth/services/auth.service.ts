import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject, map } from 'rxjs';
import { SessionStorageService } from './session-storage.service';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  constructor(
    private http: HttpClient,
    private sessionStorageService: SessionStorageService,
    private router: Router
  ) {}
  login(payload: any) {
    this.http
      .post('http://localhost:4000/login', payload)
      .pipe(
        map((data: any) => {
          this.sessionStorageService.setToken(data.result);
          this.isAuthorised$$.next(data.successful);
          this.router.navigate(['/courses']);
          return data;
        })
      )
      .subscribe();
  }
  register(payload: any) {
    this.http
      .post('http://localhost:4000/register', payload)
      .pipe(
        map((data) => {
          console.log('data ', data);
          return data;
        })
      )
      .subscribe();
  }
  logout() {
    this.http
      .delete('http://localhost:4000/logout')
      .pipe(
        map((data) => {
          this.isAuthorised$$.next(false);
          this.sessionStorageService.deleteToken();
          return data;
        })
      )
      .subscribe();
  }

  private isAuthorised$$ = new BehaviorSubject(
    !!this.sessionStorageService.getToken()
  );
  public isAuthorised$ = this.isAuthorised$$.asObservable();
}
