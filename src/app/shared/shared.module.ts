import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { HeaderComponent } from './components/header/header.component';
import { ButtonComponent } from './components/button/button.component';
import { InfoComponent } from './components/info/info.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { EmailDirective } from './directives/email.directive';
import { CustomDurationPipe } from './pipes/custom-duration.pipe';
import { TimeFormatterPipe } from './pipes/time-formatter.pipe';
import { SearchComponent } from './components/search/search.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CourseFormComponent } from './components/course-form/course-form.component';

const COMPONENTS = [
  HeaderComponent,
  ButtonComponent,
  InfoComponent,
  SearchComponent,
  CourseFormComponent,
];

@NgModule({
  declarations: [
    COMPONENTS,
    EmailDirective,
    CustomDurationPipe,
    TimeFormatterPipe,
    SearchComponent,
    CourseFormComponent,
  ],
  imports: [CommonModule, FontAwesomeModule, FormsModule, ReactiveFormsModule],
  exports: [COMPONENTS, EmailDirective, CustomDurationPipe, TimeFormatterPipe],
  providers: [DatePipe],
})
export class SharedModule {}
