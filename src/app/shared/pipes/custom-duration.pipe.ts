import { DatePipe } from '@angular/common';
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'customDuration',
})
export class CustomDurationPipe implements PipeTransform {
  constructor(public datePipe: DatePipe) {}
  transform(value: number = 0, ...args: unknown[]): unknown {
    return this.datePipe.transform(value * 60000, 'HH:mm');
  }
}
