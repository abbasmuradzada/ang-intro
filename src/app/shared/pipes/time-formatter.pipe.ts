import { DatePipe } from '@angular/common';
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'timeFormatter',
})
export class TimeFormatterPipe implements PipeTransform {
  constructor(public datePipe: DatePipe) {}
  transform(value: Date, ...args: unknown[]): unknown {
    return this.datePipe.transform(value, 'dd.MM.yyyy');
  }
}
