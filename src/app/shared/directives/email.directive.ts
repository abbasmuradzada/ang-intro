import { Directive } from '@angular/core';
import {
  AbstractControl,
  NG_VALIDATORS,
  ValidationErrors,
  Validator,
} from '@angular/forms';
import { emailValidator } from '../utils/email.validator';

@Directive({
  selector: '[isEmail]',
  providers: [
    { provide: NG_VALIDATORS, useExisting: EmailDirective, multi: true },
  ],
})
export class EmailDirective implements Validator {
  validate(control: AbstractControl): ValidationErrors | null {
    return emailValidator()(control);
  }
}
