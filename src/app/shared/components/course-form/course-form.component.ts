import { Component } from '@angular/core';
import {
  FormArray,
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-course-form',
  templateUrl: './course-form.component.html',
  styleUrls: ['./course-form.component.scss'],
})
export class CourseFormComponent {
  CourseForm: FormGroup = new FormGroup({
    title: new FormControl('', [Validators.required]),
    describtion: new FormControl('', [Validators.required]),
    authors: this.formBuilder.array([]),
    duration: new FormControl(null, [Validators.required, Validators.min(0)]),
    author: new FormControl(null, [Validators.required]),
  });
  id: string | null = '';

  // addAuthor(){

  // }
  authorTerm = '';

  ngOnInit() {
    this.activatedRoute.paramMap.subscribe((paramMap) => {
      let id = paramMap.get('id');
      if (id?.toLowerCase() === 'all') {
        id = '';
      }
      this.id = id;
    });
  }

  onSubmit() {
    console.log('submit ', this.CourseForm);
  }

  addNewAuthor() {
    const newAuthor = this.CourseForm.controls['author'];
    if (!newAuthor.value) {
      return;
    }
    const answerControl = new FormControl(newAuthor.value);
    this.authors.push(answerControl);
    newAuthor.setValue('');
  }

  removeAuthor(index: number) {
    this.authors.removeAt(index);
  }

  get authors() {
    return this.CourseForm.controls['authors'] as FormArray;
  }

  constructor(
    private formBuilder: FormBuilder,
    private activatedRoute: ActivatedRoute
  ) {}
}
