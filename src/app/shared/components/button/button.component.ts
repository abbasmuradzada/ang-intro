import { Component, Input } from '@angular/core';
import { faFilm, faEdit, faRemove } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.scss'],
})
export class ButtonComponent {
  @Input() item?: string;
  @Input() icon?: any;

  icons: any = {
    filmIcon: faFilm,
    edit: faEdit,
    remove: faRemove,
  };
}
