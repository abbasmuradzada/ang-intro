import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss'],
})
export class SearchComponent {
  term: '';
  @Input() placeholder: string;
  @Output() searchEvent = new EventEmitter<string>();

  actionedEmitted(actionType: string) {
    this.searchEvent.emit(actionType);
  }
}
