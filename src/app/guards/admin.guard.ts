import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  CanActivate,
  Router,
  RouterStateSnapshot,
  UrlTree,
} from '@angular/router';
import { Observable } from 'rxjs';
import { UserStoreService } from '../user/services/user-store.service';

@Injectable()
export class AdminGuard implements CanActivate {
  constructor(
    private userStoreService: UserStoreService,
    private router: Router
  ) {}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ):
    | Observable<boolean | UrlTree>
    | Promise<boolean | UrlTree>
    | boolean
    | UrlTree {
    let isAdmin = true;
    // I thinks it needs to work asynchronously (not sure, can come back to here)
    this.userStoreService.getUser().subscribe();
    this.userStoreService.isAdmin$.subscribe((res) => {
      isAdmin = res;
    });
    if (!isAdmin) {
      this.router.navigate(['/courses']);
    }
    return true;
  }
}
