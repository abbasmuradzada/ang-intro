import { ActionReducerMap } from '@ngrx/store';
import * as coursesReducer from './courses/courses.reducer';
import { CoursesEffects } from './courses/courses.effects';

interface State {}

export const reducers: ActionReducerMap<State> = {
  [coursesReducer.coursesFeatureKey]: coursesReducer.coursesReducer,
};

export const effects = CoursesEffects;
