import { createSelector } from '@ngrx/store';
import { CoursesState } from './courses.reducer';

export const selectState = (state: CoursesState) => state;

export const isAllCoursesLoadingSelector = createSelector(
  selectState,
  (state: CoursesState) => state.isAllCoursesLoading
);

export const isSearchingStateSelector = createSelector(
  selectState,
  (state: CoursesState) => state.isSearchState
);

export const isSingleCourseLoadingSelector = createSelector(
  selectState,
  (state: CoursesState) => state.isSingleCourseLoading
);

export const getErrorMessage = createSelector(
  selectState,
  (state: CoursesState) => state.errorMessage
);

export const getCourse = createSelector(
  selectState,
  (state: CoursesState) => state.course
);

export const getAllCourses = createSelector(
  selectState,
  (state: CoursesState) => state.allCourses
);

export const getCourses = createSelector(
  selectState,
  (state: CoursesState) => state.allCourses
);
