import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { EMPTY, of } from 'rxjs';
import { map, exhaustMap, catchError, mergeMap } from 'rxjs/operators';
import { CoursesService } from 'src/app/services/courses.service';
import {
  REQUEST_ALL_COURSES,
  REQUEST_ALL_COURSES_FAIL,
  REQUEST_ALL_COURSES_SUCCESS,
  REQUEST_DELETE_COURSE,
  REQUEST_DELETE_COURSE_FAIL,
  REQUEST_DELETE_COURSE_SUCCESS,
  REQUEST_EDIT_COURSE,
  REQUEST_EDIT_COURSE_FAIL,
  REQUEST_EDIT_COURSE_SUCCESS,
} from './courses.actions';
import { CoursesStateFacade } from './courses.facade';

@Injectable()
export class CoursesEffects {
  getAll$ = createEffect(() =>
    this.actions$.pipe(
      ofType(REQUEST_ALL_COURSES),
      mergeMap(() =>
        this.coursesService.getAll().pipe(
          map((courses) => ({
            type: REQUEST_ALL_COURSES_SUCCESS,
            payload: courses,
          })),
          catchError(() => of({ type: REQUEST_ALL_COURSES_FAIL }))
        )
      )
    )
  );

  deleteCourse$ = createEffect(() =>
    this.actions$.pipe(
      ofType(REQUEST_DELETE_COURSE),
      mergeMap((action: any) =>
        this.coursesService.deleteCourse(action.id).pipe(
          map(() => ({
            type: REQUEST_DELETE_COURSE_SUCCESS,
          })),
          catchError(() => of({ type: REQUEST_DELETE_COURSE_FAIL }))
        )
      )
    )
  );

  editCourse$ = createEffect(() =>
    this.actions$.pipe(
      ofType(REQUEST_EDIT_COURSE),
      mergeMap((action: any) =>
        this.coursesService.deleteCourse(action.payload).pipe(
          map(() => ({
            type: REQUEST_EDIT_COURSE_SUCCESS,
          })),
          catchError(() => of({ type: REQUEST_EDIT_COURSE_FAIL }))
        )
      )
    )
  );

  constructor(
    private actions$: Actions,
    private coursesService: CoursesService,
    coursesStateFacade: CoursesStateFacade
  ) {}
}
