import { state } from '@angular/animations';
import { Action, createReducer, on } from '@ngrx/store';
import { reduce } from 'rxjs';
import {
  requestAllCourses,
  requestAllCoursesFail,
  requestAllCoursesSuccess,
  requestCreateCourse,
  requestCreateCourseFail,
  requestCreateCourseSuccess,
  requestDeleteCourse,
  requestDeleteCourseFail,
  requestDeleteCourseSuccess,
  requestEditCourse,
  requestEditCourseFail,
  requestEditCourseSuccess,
  requestFilteredCourses,
  requestFilteredCoursesFail,
  requestFilteredCoursesSuccess,
  requestSingleCourse,
  requestSingleCourseFail,
  requestSingleCourseSuccess,
} from './courses.actions';

export const coursesFeatureKey = 'coursesStore';

interface ICourse {}

export interface CoursesState {
  allCourses: ICourse[];
  course: ICourse | null;
  isAllCoursesLoading: boolean;
  isSingleCourseLoading: boolean;
  isSearchState: boolean;
  errorMessage: null | string;
}

const initialState: CoursesState = {
  allCourses: [],
  course: null,
  isAllCoursesLoading: false,
  isSingleCourseLoading: false,
  isSearchState: false,
  errorMessage: null,
};

const reducers = createReducer(
  initialState,
  on(requestAllCourses, (state, action: any) => ({
    ...state,
    allCourses: action.payload,
  })),
  on(requestAllCoursesSuccess, (state) => ({ ...state })),
  on(requestAllCoursesFail, (state) => ({ ...state })),
  on(requestSingleCourse, (state) => ({ ...state })),
  on(requestSingleCourseSuccess, (state) => ({ ...state })),
  on(requestSingleCourseFail, (state) => ({ ...state })),
  on(requestFilteredCourses, (state) => ({ ...state })),
  on(requestFilteredCoursesSuccess, (state) => ({ ...state })),
  on(requestFilteredCoursesFail, (state) => ({ ...state })),
  on(requestDeleteCourse, (state) => ({ ...state })),
  on(requestDeleteCourseSuccess, (state) => ({ ...state })),
  on(requestDeleteCourseFail, (state) => ({ ...state })),
  on(requestEditCourse, (state) => ({ ...state })),
  on(requestEditCourseSuccess, (state) => ({ ...state })),
  on(requestEditCourseFail, (state) => ({ ...state })),
  on(requestCreateCourse, (state) => ({ ...state })),
  on(requestCreateCourseSuccess, (state) => ({ ...state })),
  on(requestCreateCourseFail, (state) => ({ ...state }))
);

export const coursesReducer = (
  state: CoursesState,
  action: Action
): CoursesState => reducers(state, action);
