import { Injectable } from '@angular/core';

import { select, Store } from '@ngrx/store';

import { CoursesState } from './courses.reducer';
import {
  isAllCoursesLoadingSelector,
  isSingleCourseLoadingSelector,
  isSearchingStateSelector,
  getCourse,
  getAllCourses,
  getCourses,
  getErrorMessage,
} from './courses.selectors';
import {
  requestAllCourses,
  requestCreateCourse,
  requestDeleteCourse,
  requestEditCourse,
  requestFilteredCourses,
  requestSingleCourse,
} from './courses.actions';

@Injectable({
  providedIn: 'root',
})
export class CoursesStateFacade {
  isAllCoursesLoading$ = this.store.pipe(select(isAllCoursesLoadingSelector));
  isSingleCourseLoading$ = this.store.pipe(
    select(isSingleCourseLoadingSelector)
  );
  isSearchingState$ = this.store.pipe(select(isSearchingStateSelector));
  courses$ = this.store.pipe(select(getCourses));
  course$ = this.store.pipe(select(getCourse));
  allCourses$ = this.store.pipe(select(getAllCourses));
  errorMEssage$ = this.store.pipe(select(getErrorMessage));

  constructor(private store: Store<CoursesState>) {}

  getAllCourses() {
    this.store.dispatch(requestAllCourses());
  }

  getSingleCourse(id: string) {
    this.store.dispatch(requestSingleCourse({ id }));
  }

  getFolteredCourses(searchValue: string) {
    this.store.dispatch(requestFilteredCourses({ searchValue }));
  }

  editCourse(body: string, id: string) {
    this.store.dispatch(requestEditCourse({ id, body }));
  }

  createCourse(body: string) {
    this.store.dispatch(requestCreateCourse({ body }));
  }

  deleteCourse(id: string) {
    this.store.dispatch(requestDeleteCourse({ id }));
  }
}
