import { Action, createAction, props } from '@ngrx/store';

// all courses action types
export const REQUEST_ALL_COURSES = '[all Courses] Request All Courses';
export const REQUEST_ALL_COURSES_SUCCESS =
  '[all Courses] Request All Courses Success';
export const REQUEST_ALL_COURSES_FAIL =
  '[all Courses] Request All Courses Fail';

// individual course action types
export const REQUEST_SINGLE_COURSE = '[infividual course] Request Sigle course';
export const REQUEST_SINGLE_COURSE_SUCCESS =
  '[infividual course] Request Sigle course Success';
export const REQUEST_SINGLE_COURSE_FAIL =
  '[infividual course] Request Sigle course Fail';

// filtered courses action types
export const REQUEST_FILTERED_COURSES =
  '[filtered Courses] Request filtered Courses';
export const REQUEST_FILTERED_COURSES_SUCCESS =
  '[filtered Courses] Request filtered Courses Success';
export const REQUEST_FILTERED_COURSES_FAIL =
  '[filtered Courses] Request filtered Courses Fail';

// delete course action types
export const REQUEST_DELETE_COURSE = '[delete course] Request delete course';
export const REQUEST_DELETE_COURSE_SUCCESS =
  '[delete course] Request delete course Success';
export const REQUEST_DELETE_COURSE_FAIL =
  '[delete course] Request delete course Fail';

// edit course action types
export const REQUEST_EDIT_COURSE = '[edit course] Request edit course';
export const REQUEST_EDIT_COURSE_SUCCESS =
  '[edit course] Request edit course Success';
export const REQUEST_EDIT_COURSE_FAIL =
  '[edit course] Request edit course Fail';

// create course action types
export const REQUEST_CREATE_COURSE = '[create course] Request create course';
export const REQUEST_CREATE_COURSE_SUCCESS =
  '[create course] Request create course Success';
export const REQUEST_CREATE_COURSE_FAIL =
  '[create course] Request create course Fail';

// all courses actions
export const requestAllCourses = createAction(REQUEST_ALL_COURSES);
export const requestAllCoursesSuccess = createAction(
  REQUEST_ALL_COURSES_SUCCESS,
  props<{ courses: any[] }>()
);
export const requestAllCoursesFail = createAction(REQUEST_ALL_COURSES_FAIL);

// individual course actions
export const requestSingleCourse = createAction(
  REQUEST_SINGLE_COURSE,
  props<{ id: string }>()
);
export const requestSingleCourseSuccess = createAction(
  REQUEST_SINGLE_COURSE_SUCCESS,
  props<{ course: any[] }>()
);
export const requestSingleCourseFail = createAction(REQUEST_SINGLE_COURSE_FAIL);

// filtered courses actions
export const requestFilteredCourses = createAction(
  REQUEST_FILTERED_COURSES,
  props<{ searchValue: string }>()
);
export const requestFilteredCoursesSuccess = createAction(
  REQUEST_FILTERED_COURSES_SUCCESS
);
export const requestFilteredCoursesFail = createAction(
  REQUEST_FILTERED_COURSES_FAIL
);

// delete course actions
export const requestDeleteCourse = createAction(
  REQUEST_DELETE_COURSE,
  props<{ id: string }>()
);
export const requestDeleteCourseSuccess = createAction(
  REQUEST_DELETE_COURSE_SUCCESS
);
export const requestDeleteCourseFail = createAction(REQUEST_DELETE_COURSE_FAIL);

// edit course actions
export const requestEditCourse = createAction(
  REQUEST_EDIT_COURSE,
  props<{ body: any; id: string }>()
);
export const requestEditCourseSuccess = createAction(
  REQUEST_EDIT_COURSE_SUCCESS
);
export const requestEditCourseFail = createAction(REQUEST_EDIT_COURSE_FAIL);

// create course actions
export const requestCreateCourse = createAction(
  REQUEST_CREATE_COURSE,
  props<{ body: any }>()
);
export const requestCreateCourseSuccess = createAction(
  REQUEST_CREATE_COURSE_SUCCESS
);
export const requestCreateCourseFail = createAction(REQUEST_CREATE_COURSE_FAIL);

// export type Actions
