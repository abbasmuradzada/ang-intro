import {
  HttpClient,
  HttpEvent,
  HttpHandler,
  HttpRequest,
  HttpResponse,
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, map, Observable } from 'rxjs';
import { CoursesService } from './courses.service';

@Injectable({
  providedIn: 'root',
})
export class CoursesStoreService {
  constructor(private coursesService: CoursesService) {}
  private requests: HttpRequest<any>[] = [];
  removeRequest(req: HttpRequest<any>) {
    const i = this.requests.indexOf(req);
    this.requests.splice(i, 1);
    this.isLoading$$.next(this.requests.length > 0);
  }

  getAll() {
    return this.coursesService.getAll().pipe(
      map((res: any) => {
        this.courses$$.next(res.result);
        return res.result;
      })
    );
  }

  searchCourses(title: string) {
    return this.coursesService.searchCourses(title).pipe(
      map((res: any) => {
        this.courses$$.next(res);
        return res;
      })
    );
  }

  getCourse(id: string) {
    return this.coursesService.getCourse(id).pipe(
      map((res: any) => {
        return res;
      })
    );
  }

  deleteCourse(id: string) {
    return this.coursesService.deleteCourse(id).pipe(
      map((res: any) => {
        // this.courses$$.next(res);
        return res;
      })
    );
  }

  createCourse(payload: any) {
    return this.coursesService.createCourse(payload).pipe(
      map((res: any) => {
        // this.courses$$.next(res);
        return res;
      })
    );
  }

  editCourse(id: string, payload: any) {
    return this.coursesService.editCourse(id, payload).pipe(
      map((res: any) => {
        // this.courses$$.next(res);
        return res;
      })
    );
  }

  private courses$$ = new BehaviorSubject([]);
  private isLoading$$ = new BehaviorSubject(false);
  public courses$ = this.courses$$.asObservable();
  public isLoading$ = this.isLoading$$.asObservable();

  logCourses() {
    this.courses$$.subscribe(console.log);
  }
}
