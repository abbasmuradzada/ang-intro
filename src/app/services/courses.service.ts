import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class CoursesService {
  constructor(private http: HttpClient) {}
  getAll() {
    return this.http.get('http://localhost:4000/courses/all');
  }
  createCourse(payload: any) {
    return this.http
      .post(`http://localhost:4000/courses/add`, payload)
      .pipe(map((res: any) => res.result));
  }
  editCourse(id: string, payload: any) {
    return this.http
      .put(`http://localhost:4000/courses/${id}`, payload)
      .pipe(map((res: any) => res.result));
  }
  getCourse(id: string) {
    return this.http
      .get(`http://localhost:4000/courses/${id}`)
      .pipe(map((res: any) => res.result));
  }
  deleteCourse(id: string) {
    return this.http
      .delete(`http://localhost:4000/courses/${id}`)
      .pipe(map((res: any) => res.result));
  }
  searchCourses(title: string) {
    return this.http
      .get(`http://localhost:4000/courses/filter?title=${title}`)
      .pipe(map((res: any) => res.result));
  }
}
