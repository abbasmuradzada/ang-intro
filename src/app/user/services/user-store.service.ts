import { Injectable } from '@angular/core';
import { BehaviorSubject, map } from 'rxjs';
import { UserService } from './user.service';

@Injectable({
  providedIn: 'root',
})
export class UserStoreService {
  constructor(private userService: UserService) {}

  getUser() {
    return this.userService.getUser().pipe(
      map((res: any) => {
        this.name$$.next(res.result.name);
        this.isAdmin$$.next(res.result.role !== 'user');
        return res.result;
      })
    );
  }

  private name$$ = new BehaviorSubject('');
  private isAdmin$$ = new BehaviorSubject(false);
  public name$ = this.name$$.asObservable();
  public isAdmin$ = this.isAdmin$$.asObservable();
}
