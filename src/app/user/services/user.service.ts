import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { SessionStorageService } from 'src/app/auth/services/session-storage.service';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  constructor(
    private http: HttpClient,
    private sessionStorageService: SessionStorageService
  ) {}

  getUser() {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      Authorization: this.sessionStorageService.getToken() ?? '',
    });
    return this.http.get('http://localhost:4000/users/me', {
      headers: headers,
    });
  }
}
