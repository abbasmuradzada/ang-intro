import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './features/login/login.component';
import { RegistrationComponent } from './features/registration/registration.component';
import { CoursesComponent } from './features/courses/courses.component';
import { CourseComponent } from './features/course/course.component';
import { CourseFormComponent } from './shared/components';
import { AdminGuard } from './guards/admin.guard';
import { NotAuthorizedGuard } from './auth/guards/not-authorized.guard';
import { AuthorizedGuard } from './auth/guards/authorized.guard';

const routes: Routes = [
  { path: '', redirectTo: '/courses', pathMatch: 'full' },
  {
    path: 'login',
    canActivate: [NotAuthorizedGuard],
    component: LoginComponent,
    // loadComponent: () =>
    //   import('./features/login/login.component').then((m) => m.LoginComponent),
  },
  {
    path: 'registration',
    component: RegistrationComponent,
    // loadComponent: () =>
    //   import('./features/registration/registration.component').then(
    //     (m) => m.RegistrationComponent
    //   ),
    canActivate: [NotAuthorizedGuard],
  },
  {
    path: 'courses',
    component: CoursesComponent,
    // loadChildren: () =>
    //   import('./features/courses/').then(
    //     (m) => m.CoursesComponent
    //   ),
    canLoad: [AuthorizedGuard],
    children: [
      {
        path: 'add',
        title: 'add',
        component: CourseFormComponent,
        canActivate: [AdminGuard],
        canLoad: [AuthorizedGuard],
      },
      {
        path: 'edit/:id',
        title: 'edit',
        component: CourseFormComponent,
        canActivate: [AdminGuard],
        canLoad: [AuthorizedGuard],
      },
      {
        path: ':id',
        title: 'course details',
        component: CourseComponent,
        canLoad: [AuthorizedGuard],
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [AdminGuard, NotAuthorizedGuard, AuthorizedGuard],
})
export class AppRoutingModule {}
