import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CoursesComponent } from './courses.component';
import { CourseModule } from '../course/course.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { AppRoutingModule } from 'src/app/app-routing.module';
import { RouterModule, Routes } from '@angular/router';
import { AuthorizedGuard } from 'src/app/auth/guards/authorized.guard';
import { CourseFormComponent } from 'src/app/shared/components';
import { AdminGuard } from 'src/app/guards/admin.guard';
import { CourseComponent } from '../course/course.component';
import { NotAuthorizedGuard } from 'src/app/auth/guards/not-authorized.guard';

// const routes: Routes = [
//   {
//     path: 'add',
//     title: 'add',
//     component: CourseFormComponent,
//     canActivate: [AdminGuard],
//     canLoad: [AuthorizedGuard],
//   },
//   {
//     path: 'edit/:id',
//     title: 'edit',
//     component: CourseFormComponent,
//     canActivate: [AdminGuard],
//     canLoad: [AuthorizedGuard],
//   },
//   {
//     path: ':id',
//     title: 'course details',
//     component: CourseComponent,
//     canLoad: [AuthorizedGuard],
//   },
// ];

@NgModule({
  declarations: [CoursesComponent],
  exports: [CoursesComponent, RouterModule],
  imports: [CommonModule, CourseModule, SharedModule, AppRoutingModule],
  providers: [AdminGuard, NotAuthorizedGuard, AuthorizedGuard],
})
export class CoursesModule {}
