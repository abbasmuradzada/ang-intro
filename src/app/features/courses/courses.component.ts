import { HttpClient } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { map } from 'rxjs';
import { AuthService } from 'src/app/auth/services/auth.service';
import { CoursesStoreService } from 'src/app/services/courses-store.service';
import { CoursesService } from 'src/app/services/courses.service';
import { UserStoreService } from 'src/app/user/services/user-store.service';

@Component({
  selector: 'app-courses',
  templateUrl: './courses.component.html',
  styleUrls: ['./courses.component.scss'],
})
export class CoursesComponent {
  constructor(
    private coursesStoreService: CoursesStoreService,
    private userStoreService: UserStoreService,
    private authService: AuthService
  ) {}
  @Input() courses: any[];
  @Input() editable: boolean;

  posts: any;

  logOut() {
    this.authService.logout();
  }

  actionLogger(action: string) {
    console.log(`${action} emitted`);
  }
  searchItem(searchedItem: string) {
    if (searchedItem?.length > 0) {
      this.coursesStoreService
        .searchCourses(searchedItem)
        .subscribe((courses) => {
          this.courses = courses;
        });
    }
  }

  loading = this.coursesStoreService.isLoading$;

  ngOnInit() {
    this.posts = this.coursesStoreService.getAll().subscribe((courses) => {
      this.courses = courses;
    });

    this.userStoreService
      .getUser()
      .subscribe((user) => console.log('user is: ', user));
  }
}
