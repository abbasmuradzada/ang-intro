import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AuthService } from 'src/app/auth/services/auth.service';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.scss'],
})
export class RegistrationComponent {
  constructor(private authService: AuthService) {}
  model: any = {};
  SignupForm: FormGroup = new FormGroup({
    name: new FormControl('', [Validators.required, Validators.minLength(6)]),
    email: new FormControl('', [Validators.required]),
    password: new FormControl('', [Validators.required]),
  });

  onSubmit() {
    this._updateValueAndValidity();

    if (this.SignupForm.invalid) {
      this.SignupForm.markAllAsTouched();
    } else {
      this.authService.register({
        name: this.SignupForm.value.name,
        email: this.SignupForm.value.email,
        password: this.SignupForm.value.password,
      });
    }
  }

  private _updateValueAndValidity() {
    for (let controlName in this.SignupForm.controls) {
      this.SignupForm.controls[controlName].updateValueAndValidity();
    }
  }
}
