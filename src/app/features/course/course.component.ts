import { Output, EventEmitter } from '@angular/core';
import { Component, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CoursesStoreService } from 'src/app/services/courses-store.service';

@Component({
  selector: 'app-course',
  templateUrl: './course.component.html',
  styleUrls: ['./course.component.scss'],
})
export class CourseComponent {
  @Input() title: string;
  @Input() desc: string;
  @Input() creationDate: Date;
  @Input() duration: number;
  @Input() authors: string[];
  @Input() editable: boolean;
  @Output() newItemEvent = new EventEmitter<string>();
  id: string | null = '';

  ngOnInit() {
    this.activatedRoute.paramMap.subscribe((paramMap) => {
      let id = paramMap.get('id');
      if (id?.toLowerCase() === 'all') {
        id = '';
      } else {
        id &&
          this.coursesStoreService
            .getCourse(id as string)
            .subscribe((course) => {
              console.log(course);

              this.title = course.title;
              this.desc = course.description;
              this.creationDate = course.creationDate;
              this.authors = course.authors;
              this.duration = course.duration;
            });
      }
    });
  }
  actionedEmitted(actionType: string) {
    this.newItemEvent.emit(actionType);
  }
  test() {
    this.newItemEvent.emit('test');
  }

  constructor(
    private activatedRoute: ActivatedRoute,
    private coursesStoreService: CoursesStoreService
  ) {}
}
