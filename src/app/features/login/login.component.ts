import { Component } from '@angular/core';
import { AuthService } from 'src/app/auth/services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent {
  constructor(private authService: AuthService) {}
  model: any = {};
  logger = () => console.log(this.model);
  onSubmit = () => {
    this.authService.login({
      email: this.model.email,
      password: this.model.password,
    });
  };
}
